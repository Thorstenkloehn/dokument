## Windows

## Einstellung

```

    setx CARGO_HOME  e:\CARGO_HOME
    setx RUSTUP_HOME  e:\RUSTUP_HOME
    setx TEMP e:\AppData\Local\Temp
    setx TMP e:\AppData\Local\Temp
    setx GOPATH d:\go
    setx Android_SDK_HOME e:\androidsdkhome
```

## Grundlagenn Programme


### Anti Virus Programmieren

* [Bitdefender](https://login.bitdefender.com/central/login.html?lang=de_DE&redirect_url=https:%2F%2Fcentral.bitdefender.com%2Factivity%3FbrowserLang%3Dde_DE)

### Browser

* [Microsoft Edge](https://www.microsoft.com/en-us/edge)
* [Google Chrome](https://www.google.de/chrome)

### Versionsverwaltungssoftware


* [Git Installieren](https://git-scm.com/)

### Allgemeine Module

* [visual Cpp Build Tools](https://visualstudio.microsoft.com/de/downloads) nur C++
* [Msys2](https://www.msys2.org/)
* [MSYS2 Packages](https://packages.msys2.org/updates)

* MSYS2 Console eingeben


```
 pacman -S mingw-w64-x86_64-toolchain base-devel mingw-w64-x86_64-pkg-config mingw-w64-x86_64-curl mingw-w64-x86_64-cmake
```

### Programmiersprachen

* [Rust](https://www.rust-lang.org/)
* [Nodejs](https://nodejs.org/en/download/) Bitte nur LTS Installieren

## IDE
* [IntelliJ IDEA](https://www.jetbrains.com/idea/) Erste Installieren und Starten
* [JetBrains-Gateway](https://www.jetbrains.com/help/idea/2021.3/remote-development-a.html#gateway>)




