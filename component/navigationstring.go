package component

import (
	"github.com/russross/blackfriday"
	"log"
	"os"
	"path/filepath"
	"strings"
)

func NavigationStringAusgabe() string {
	var inhaltString string

	err := filepath.Walk("inhalt",
		func(path string, info os.FileInfo, err error) error {
			if err != nil {
				return err
			}
			ausgabe := strings.ReplaceAll(path, "\\", "/")
			ggg := strings.Split(ausgabe, "/")
			anzahl := len(ggg)

			if anzahl == 1 {
				// navigation := `- [+ ggg[0] + http://localhost:8080/` + ausgabe + "]" + "(" + ggg[0] + "}"
				navigation := `- [` + ggg[0] + `]` + `(` + `http://localhost:8080/` + ausgabe + `) `
				inhaltString = inhaltString + navigation + "\n"
			} else {
				var xxx string
				for range ggg {

					xxx = xxx + "  "
				}
				xxx = xxx + " - "
				navigation := xxx + `[` + ggg[anzahl-1] + `]` + `(` + `http://localhost:8080/` + ausgabe + `) `
				inhaltString = inhaltString + navigation + "\n"
			}

			return nil
		})
	if err != nil {
		log.Println(err)
	}

	output := blackfriday.MarkdownBasic([]byte(inhaltString))
	return string(output)
}
