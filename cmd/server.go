/*
Copyright © 2022 NAME HERE <EMAIL ADDRESS>

*/
package cmd

import (
	"fmt"
	"github.com/spf13/viper"
	"github.com/thorstenkloehn/dokument/controller"
	"net/http"
	"os"

	"github.com/spf13/cobra"
)

// serverCmd represents the server command
var serverCmd = &cobra.Command{
	Use:   "server",
	Short: "A brief description of your command",
	Long: `A longer description that spans multiple lines and likely contains examples
and usage of using your command. For example:

Cobra is a CLI library for Go that empowers applications.
This application is a tool to generate the needed files
to quickly create a Cobra application.`,
	Run: func(cmd *cobra.Command, args []string) {
		viper.SetConfigName("config")  // Name der Konfigurationsdatei (ohne Erweiterung)
		viper.SetConfigType("yaml")    // ERFORDERLICH, wenn die Konfigurationsdatei die Erweiterung nicht im Namen hat
		viper.AddConfigPath("config/") // Pfad, um nach der Konfigurationsdatei zu suchen

		err := viper.ReadInConfig() // Find and read the config file
		if err != nil {             //Datei ist nicht Vorhanden
			fmt.Print("Erstelle eine config/config.yaml")
			os.Exit(1)
		}
		fmt.Println("config/config.yaml ist vorhanden")

		// Router erstellen

		serverController := controller.WebSeiten{}
		router := http.NewServeMux()
		fs := http.FileServer(http.Dir("./static"))
		router.Handle("/static/", http.StripPrefix("/static/", fs))
		router.HandleFunc("/", serverController.Seiten)

		fmt.Println("http://localhost:8080")
		http.ListenAndServe(":8080", router)
	},
}

func init() {
	rootCmd.AddCommand(serverCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// serverCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// serverCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
