package controller

import (
	"github.com/russross/blackfriday"
	"github.com/spf13/viper"
	"github.com/thorstenkloehn/dokument/component"
	"io/ioutil"
	"net/http"
	"path/filepath"
	"text/template"
)

type WebSeiten struct {
	WebsiteTitel string
	Titel        string
	Inhalt       string
	Navigation   string
}

var vorlagen, _ = template.ParseGlob("view/*")

func (webseiten *WebSeiten) Hallo() string {
	return "Willkommen auf meine Seite"
}

func (webseiten *WebSeiten) Seiten(w http.ResponseWriter, r *http.Request) {

	webseiten.WebsiteTitel = "Hallo Leute"

	webseiten.Titel = viper.GetString("Titel")
	webseiten.Navigation = component.NavigationStringAusgabe()

	content, _ := ioutil.ReadFile(filepath.Join(r.URL.Path[len("/"):]))
	output := blackfriday.MarkdownBasic(content)
	webseiten.Inhalt = string(output)
	vorlagen.ExecuteTemplate(w, "index.html", webseiten)
}
